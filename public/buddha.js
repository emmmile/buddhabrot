
var buddhaTypeOptions = {
  getTileUrl: function(coord, zoom) {
      var normalizedCoord = getNormalizedCoord(coord, zoom);
      //console.log(coord.x + ", " + coord.y + " > " + normalizedCoord.x + ", " + normalizedCoord.y);
      if (!normalizedCoord) {
        return null;
      }
      var bound = Math.pow(2, zoom);
      return '/buddhabrot/tiles/' + zoom + '/tile-' + normalizedCoord.x + '-' +
          normalizedCoord.y + '.png';
  },
  tileSize: new google.maps.Size(256, 256),
  maxZoom: 7,
  minZoom: 0,
  name: 'Buddhabrot',
};

var buddhaMapType = new google.maps.ImageMapType(buddhaTypeOptions);

function initialize() {
  var myLatlng = new google.maps.LatLng(0, 0);
  var mapOptions = {
    center: myLatlng,
    zoom: 1,
    disableDefaultUI: true,
    backgroundColor: '#000'
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  map.mapTypes.set('buddha', buddhaMapType);
  map.setMapTypeId('buddha');
}

// Normalizes the coords that tiles repeat across the x axis (horizontally)
// like the standard Google map tiles.
function getNormalizedCoord(coord, zoom) {
  var y = coord.y;
  var x = coord.x;

  // tile range in one direction range is dependent on zoom level
  // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
  var tileRange = 1 << zoom;

  // don't repeat across y-axis (vertically)
  if (y < 0 || y >= tileRange) {
    return null;
  }

  // don't repeat across x-axis either
  if (x < 0 || x >= tileRange) {
    return null;
  }

  return {
    x: x,
    y: y
  };
}

google.maps.event.addDomListener(window, 'load', initialize);
